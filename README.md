# Thank you Juan and Scott!

## What is something you appreciate about Juan?

Juan is always open and straight forward with a great sense of humor.

Juan trusts his team to make the right call, even when he doesn't completely agree. 

Juan is a hallmark optimist. He sees the best in every person he meets and every situation he encounters, and never leaves a room or call without elevating and inspiring those around him.

Be paranoid. Trust, but verify. Juan is always full of wisdom and I learn something from all of our discussions.

Juan makes a mean smoked turkey. I haven't had a lot of interactions with Juan but I appreciate that he owns up to past coding sins from back in the day! Says a lot about his perspective on honesty and ownership.

Juan is el verdadero consentido mio lol. Thanks for everything you do professionally and personally to take care of your people.

Juan isn't afraid to have brutally honest conversations about things that matter to his team. In times when I've had real concerns, I've always known that I could pull Juan aside and talk critically without fear of consequences or anger from him. Instead, he's welcomed my honesty and been willing to be honest with me as well, and I've always known that he would take care of me. 

Juan is brilliant but humble about it. I appreciate that he is willing to step in and help with bugs or difficult code even when he has so many bigger things going on.

Juan perfectly blends being driven for results and caring for his team.  I admire his ability to ask just the right question to crack open an issue.


Juan has the double whammy when it comes to talents. He is not only super smart but also has next level barbecue skilz

Juan in one word: Inspirational Leader.


## What is something you appreciate about Scotty?

Scotty has engineerings best intrests at heart and isn't afriad to go to bat for the team.

Scotty is not afraid to step up and make the hard decisions when warranted.

I love how Scotty listens to each team member and puts it into action. I think he is a great example for taking Extreme Ownership even when his engineers make mistakes.

Scotty looks out for his team. I'm grateful that he was willing to take a chance on me as a n00b engineer, and always goes the extra mile to make sure the team has the resources needed to succeed.

Scotty is a great example and looks out for all of us. While he continues to challenge me, he ensures that I have what I need to succeed.
#drivenbydata is this guy!

Scotty is energetic and I appreciate his leadership style of leading from the front - he doesn't ask people to do something he's not willing to (and often currently) doing himself. I feel like Scotty's got my back!

Scotty is able to inspire his engineers to think big and be better in a rare way. I am a significantly better engineer because of honest feedback I've gotten from Scotty in 1 on 1s, and he's especially always pushed me to speak my mind, to trust my instincts, and to take the initiative when I see issues. I'm grateful to have a mentor like that, and one that always will take care of me.

Scotty is one of the coolest people I've ever worked with! He's always willing to share his wisdom and experiences and I've learned so much from him already. It's clear that he genuinely cares about the success of our team and Whistic as a whole. Not only that, he's a fun, open guy to talk to!

I love that Scotty is vested in the advancement of those he oversees. From bookclub, lunch'n learn, Udemy, and conferences. Definitely investing in the future. 

Scotty is like our overwatch sniper - always ready to take out an inefficient process, or alert us when we have a high priority target to achieve.  He goes the extra mile for his team to ensure they have everything they need to succeed.


Scotty is super people oriented, fun to work and hang out with and im pretty sure he can bench press more than anyone in the office...

Scotty in one word: Motivational Leader

